/**
 * mainBottomTab
 */
const mainBottomTab = {
  homeStack: 'HomeStack',
  briefcaseStack: 'BriefcaseStack',
  exploreStack: 'ExploreStack',
  moreStack: 'MoreStack',
  postStack: 'PostStack',
};
export default mainBottomTab;

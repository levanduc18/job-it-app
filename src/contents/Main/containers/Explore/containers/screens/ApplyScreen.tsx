import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Body, Button, Container, Header, QuickView, Text, } from '@components';
import { applyObjectSelector, parseObjectSelector } from '@utils/selector';
import { profileSelector } from '@contents/Main/containers/Profile/redux/selector';
import { getIdFromParams, Global } from '@utils/appHelper';
import DocumentPicker from 'react-native-document-picker';
import axios from 'axios';
import { Linking, StatusBar, TouchableOpacity, Alert } from 'react-native';
import NavigationService from '@utils/navigation';
import { profileGetDetail } from '@contents/Main/containers/Profile/redux/slice';
import { TQuery } from '@utils/redux';
import { myJobsGetApplied } from '@contents/Main/containers/MyJobs/redux/slice';
import { jobApplies } from '../../redux/slice';
interface Props {
  appliesJob: (id: string) => any;
  id: any;
  profile: any;
  getDetailProfile: any;
  getListApplied: any;
}

interface State {
  url: any;
  checked: any;
}
class ApplyScreen extends PureComponent<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      url: "No exist CV in your profile",
      checked: true,
    };
  }
  componentDidMount() {
    const { getDetailProfile } = this.props;
    getDetailProfile();
  }
  renderCenterComponent = () => (
    <Text color="#fff" fontSize={20} fontWeight="bold">
      Apply Job
    </Text>
  );
  render() {
    const {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      appliesJob,
      profile: { data },
      getListApplied,
    } = this.props;
    if (data.profile && data.profile.cvURL && this.state.checked) {
      this.setState({ url: data.profile.cvURL })
    }
    const source = {
      uri: this.state.url,
      cache: true,
    };
    return (
      <Container>
        <StatusBar backgroundColor="transparent" />
        <Header
          backIcon
          leftColor="#fff"
          backgroundColor="#6ca9e6"
          height={100}
          centerComponent={this.renderCenterComponent()}
        />
        <Body backgroundColor="#fff">
          <Text fontSize={18} center marginTop={40} color="#000">
            Following profile will be sent to the recruiter
            </Text>
          <QuickView
            marginTop={50}
            paddingHorizontal={20}
            paddingVertical={30}
            backgroundColor="#6ca9e6"
            style={{ flex: 10, borderRadius: 6 }}
          >
            <Text color="#fff" bold fontSize={16}>
              Attached CV:
          </Text>
            <TouchableOpacity onPress={() => Linking.openURL(this.state.url)}>
              <Text color="#fff" >
                {this.state.url}
              </Text>
            </TouchableOpacity>
            {/* <Text color="#fff" bold marginTop={40}>
              Your email:
              </Text>
            <Text color="#fff">
              {data.profile && data.profile.email ? data.profile.email : ''}
            </Text> */}
          </QuickView>
          <QuickView
            row
            center
            paddingHorizontal={20}
            paddingVertical={30}
            marginTop={20}
            style={{ flex: 5 }}
          >
            <Button
              width={100}
              backgroundColor="#6ca9e6"
              title="Upload"
              marginBottom={30}
              onPress={async () => {
                try {
                  const uploadFile = await DocumentPicker.pick({
                    type: [
                      DocumentPicker.types.pdf,
                      DocumentPicker.types.docx,
                      DocumentPicker.types.zip,
                    ],
                  });
                  const image: any = {
                    uri: uploadFile.uri,
                    type: uploadFile.type,
                    name: uploadFile.name,
                  };

                  const data = new FormData();
                  data.append('file', image);
                  const result: any = await axios.post(
                    'http://careernetwork.ml/api/v1/upload',
                    data,
                    {
                      headers: {
                        'Content-Type': 'multipart/form-data; ',
                      },
                    },
                  )
                  const { token } = Global;
                  await axios.patch(
                    'http://careernetwork.ml/api/v1/auth/me/cv',
                    {
                      cvUrl: result.data.data.url,
                    },
                    {
                      headers: {
                        Authorization: `Bearer ${token}`,
                      },
                    },
                  ).then(() => {
                    this.setState({ url: result.data.data.url, checked: false });

                  });
                  // NavigationService.goBack();
                } catch (err) {
                  // eslint-disable-next-line no-console
                  console.log('err', err);
                  if (DocumentPicker.isCancel(err)) {
                    // User cancelled the picker, exit any dialogs or menus and move on
                  } else {
                    throw err;
                  }
                }
              }
              }
            />
            <Button
              backgroundColor="#6ca9e6"
              title="Submit"
              marginBottom={30}
              paddingLeft={20}
              width={100}
              onPress={async () => {
                if (data.profile.cvURL) {
                  const { token } = Global;
                  const result: any = await axios.post(
                    `http://careernetwork.ml/api/v1/jobs/${getIdFromParams(
                      this.props,
                    )}/applies`,
                    data,
                    {
                      headers: {
                        Authorization: `Bearer ${token}`,
                      },
                    },
                  );
                  getListApplied();
                  Alert.alert("Sent resume")
                  NavigationService.goBack();
                } else {
                  Alert.alert('No attached CV?', 'Please upload your CV')
                }
              }
              }
            />
          </QuickView>
        </Body>
      </Container>

    );
  }
}

const mapStateToProps = (state: any) => ({
  profile: parseObjectSelector(applyObjectSelector(profileSelector, state)),
});

const mapDispatchToProps = (dispatch: any) => ({
  appliesJob: (id: string) => dispatch(jobApplies({ id })),
  getDetailProfile: (query?: TQuery) => dispatch(profileGetDetail({ query })),
  getListApplied: () => dispatch(myJobsGetApplied({})),
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplyScreen as any);

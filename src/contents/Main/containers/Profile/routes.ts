const profileStack = {
  index: 'ProfileStack',
  SkillScreen: 'SkillScreen',
  DetailSkillScreen: 'DetailSkillScreen',
  languageScreen: 'LanguageScreen',
  educationScreen: 'EducationScreen',
  detailEducationScreen: 'DetailEducationScreen',
};

export default profileStack;
